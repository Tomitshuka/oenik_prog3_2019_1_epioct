﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Paintshop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using Paintshop.Data;
    using Paintshop.Logic;
    using Paintshop.Repository;

    /// <summary>
    /// Test logic with UnitTest and Moq.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<Logic> mockedLogic;
        private Mock<IRepository<Shop>> mockedShop;
        private Mock<IRepository<Item>> mockedItem;
        private Mock<IRepository<Customer>> mockedCustomer;

        /// <summary>
        /// Sets up the mocked repositories before the tests.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Logic m = Mock.Of<Logic>();
            this.mockedLogic = Mock.Get(m);
            this.mockedShop = new Mock<IRepository<Shop>>();
            this.mockedItem = new Mock<IRepository<Item>>();
            this.mockedCustomer = new Mock<IRepository<Customer>>();
            List<Customer> customers = new List<Customer>()
            {
                new Customer() { Id = 1, Name = "Customer_Janos", Date_of_birth = DateTime.Parse("05/29/2015"), Mothers_name = "Mother_name", Place_of_birth = "Place", Regular_customer = 0 },
                new Customer() { Id = 2, Name = "Customer_Bela", Date_of_birth = DateTime.Parse("05/29/2015"), Mothers_name = "Mother_name", Place_of_birth = "Place", Regular_customer = 0 },
                new Customer() { Id = 3, Name = "Customer_Istvan", Date_of_birth = DateTime.Parse("05/29/2015"), Mothers_name = "Mother_name", Place_of_birth = "Place", Regular_customer = 1 },
            };
        }

        /// <summary>
        /// Test1.
        /// </summary>
        [Test]
        public void Test1()
        {
            Assert.That(this.mockedLogic.Object.DeleteFromItem(1), Is.EqualTo("Instance removed successfully!"));
        }

        /// <summary>
        /// Test2.
        /// </summary>
        [Test]
        public void Test2()
        {
            Assert.That(this.mockedLogic.Object.DeleteFromCustomers(1), Is.EqualTo("Instance removed successfully!"));
        }

        /// <summary>
        /// Test3.
        /// </summary>
        [Test]
        public void Test3()
        {
            Assert.That(this.mockedLogic.Object.DeleteFromShop(1), Is.EqualTo("Instance removed successfully!"));
        }

        /// <summary>
        /// Test5.
        /// </summary>
        [Test]
        public void Test4()
        {
            Assert.That(this.mockedLogic.Object.GetAllFromCustomers, !Is.Null);
        }

        /// <summary>
        /// Test6.
        /// </summary>
        [Test]
        public void Test5()
        {
            Assert.That(this.mockedLogic.Object.GetAllFromItem, !Is.Null);
        }

        /// <summary>
        /// Test7.
        /// </summary>
        [Test]
        public void Test6()
        {
            Assert.That(this.mockedLogic.Object.GetAllFromShop, !Is.Null);
        }

        /// <summary>
        /// Test8.
        /// </summary>
        [Test]
        public void Test7()
        {
            Assert.That(this.mockedLogic.Object.DeleteFromShop(1), !Is.EqualTo("Instance removed successfully!"));
        }

        /// <summary>
        /// Test8.
        /// </summary>
        [Test]
        public void Test8()
        {
            List<string> test = new List<string>();
            test.Add("5");
            test.Add("name");
            test.Add("05/29/2015");
            test.Add("place");
            test.Add("0");
            test.Add("0");

            Assert.That(this.mockedLogic.Object.CreateToCustomers(test), !Is.Null);
        }

        /// <summary>
        /// Test9.
        /// </summary>
        [Test]
        public void Test9()
        {
            List<string> test = new List<string>();
            test.Add("8");
            test.Add("address");
            test.Add("settlemant");
            test.Add("zippcode");
            test.Add("0");
            test.Add("0");

            Assert.That(this.mockedLogic.Object.CreateToShop(test), !Is.Null);
        }
    }
}
