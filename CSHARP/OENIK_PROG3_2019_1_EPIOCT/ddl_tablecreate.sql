﻿CREATE TABLE [dbo].[Aru] (
    [Termek_azonosito] INT          NOT NULL,
    [Termek_nev]       VARCHAR (50) NULL,
    [Ar]               INT          NULL,
    [Lejar]            BIT          NULL,
    [Lejarat_datuma]   DATE         NULL,
    PRIMARY KEY CLUSTERED ([Termek_azonosito] ASC)
);

CREATE TABLE [dbo].[Bolt] (
    [Bolt_azonosito] INT          NOT NULL,
    [Telepules]      VARCHAR (50) NULL,
    [Iranyitosam]    INT          NULL,
    [Cim]            VARCHAR (50) NULL,
    [Boltvezeto]     VARCHAR (50) NULL,
    [Nyitvatartas]   VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Bolt_azonosito] ASC)
);

CREATE TABLE [dbo].[Vasarlas] (
    [Vasarlas_azonosito] INT          NOT NULL,
    [Termek_azonosito]   INT          NULL,
    [Bolt_azonosito]     INT          NULL,
    [Nev]                VARCHAR (50) NULL,
    [Szuletesi_datum]    DATE         NULL,
    [Anyja_neve]         VARCHAR (50) NULL,
    [Vasarlas_datuma]    DATE         NULL,
    [Mennyiseg]          INT          NULL,
    [Kert_szamlat]       BIT          NULL,
    PRIMARY KEY CLUSTERED ([Vasarlas_azonosito] ASC)
);

CREATE TABLE [dbo].[Vevo] (
    [Nev]             VARCHAR (50) NOT NULL,
    [Szuletesi_datum] DATE         NOT NULL,
    [Szuletesi_hely]  VARCHAR (50) NOT NULL,
    [Anyja_neve]      VARCHAR (50) NOT NULL,
    [Torzsvasarol]    BIT          NULL,
    CONSTRAINT [PK_Vevo] PRIMARY KEY CLUSTERED ([Anyja_neve] ASC, [Nev] ASC, [Szuletesi_datum] ASC)
);

