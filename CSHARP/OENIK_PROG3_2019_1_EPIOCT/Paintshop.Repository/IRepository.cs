﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Paintshop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface to Entity classes. Implemented by all.
    /// </summary>
    /// <typeparam name="T">Generic type, refered to Shop, Customer etc.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        ///  Insert method.
        /// </summary>
        /// <param name="parameters">Data to insert.</param>
        /// <returns>Return list.</returns>
        string Insert(List<string> parameters);

        /// <summary>
        /// Delete method.
        /// </summary>
        /// <param name="id">ID of the instance.</param>
        /// <returns>Return list.</returns>
        string Delete(int id);

        /// <summary>
        /// Update.
        /// </summary>
        /// <param name="parameters">New data.</param>
        /// <returns>Return list.</returns>
        string Update(List<string> parameters);

        /// <summary>
        /// Get all data from table.
        /// </summary>
        /// <returns>All data from table.</returns>
        IEnumerable<T> GetAll();
    }
}
