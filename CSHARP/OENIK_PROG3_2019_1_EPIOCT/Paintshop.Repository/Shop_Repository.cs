﻿// <copyright file="Shop_Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Paintshop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Paintshop.Data;

    /// <summary>
    /// Repository to manage shops table.
    /// </summary>
    public class Shop_Repository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Shop_Repository"/> class.
        /// </summary>
        public Shop_Repository()
        {
            this.Db = new Paintshop_dbEntities1();
        }

        /// <summary>
        /// Gets or sets database instance.
        /// </summary>
        private Paintshop_dbEntities1 Db { get; set; }

        /// <summary>
        /// Delete shop.
        /// </summary>
        /// <param name="id">ID to instance.</param>
        /// <returns>Returne message.</returns>
        public string Delete(int id)
        {
            // Should check if 'id' is a valid id to a instance.
            using (Paintshop_dbEntities1 db = new Paintshop_dbEntities1())
            {
                var delete = db.Shops.Find(id);
                try
                {
                    db.Shops.Remove(delete);
                    db.SaveChanges();
                    return "Instance removed successfully!";
                }
                catch (Exception e)
                {
                    return "Error: " + e.Message;
                }
            }
        }

        /// <summary>
        /// Querry all instance.
        /// </summary>
        /// <returns>IEnumerabley(List).</returns>
        public IEnumerable<Shop> GetAll()
        {
            List<Shop> result = this.Db.Shops.ToList();
            return result;
        }

        /// <summary>
        /// Insert shop.
        /// </summary>
        /// <param name="parameters">Given parameters.</param>
        /// <returns>Return message.</returns>
        public string Insert(List<string> parameters)
        {
            Shop newShop = new Shop
            {
                Id = int.Parse(parameters.ElementAt(0)),
                Address = parameters.ElementAt(1),
                Settlement = parameters.ElementAt(2),
                Zipcode = parameters.ElementAt(3),
                Opening_hours = parameters.ElementAt(4),
                Shopkeeper = parameters.ElementAt(5),
                Purchases = null,
            };
            try
            {
                this.Db.Shops.Add(newShop);
                this.Db.SaveChanges();
                return "Instance addedd successfully!";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        /// <summary>
        /// Update an instance.
        /// </summary>
        /// <param name="parameters">Given parameters.</param>
        /// <returns>Return message.</returns>
        public string Update(List<string> parameters)
        {
            Shop newShop = new Shop
            {
                Id = int.Parse(parameters.ElementAt(0)),
                Address = parameters.ElementAt(1),
                Settlement = parameters.ElementAt(2),
                Zipcode = parameters.ElementAt(3),
                Opening_hours = parameters.ElementAt(4),
                Shopkeeper = parameters.ElementAt(5),
                Purchases = null,
            };
            try
            {
                var update = this.Db.Shops.SingleOrDefault(x => x.Id == newShop.Id);
                if (update != null)
                {
                    update.Address = newShop.Address;
                    update.Settlement = newShop.Settlement;
                    update.Zipcode = newShop.Zipcode;
                    update.Opening_hours = newShop.Opening_hours;
                    update.Shopkeeper = newShop.Shopkeeper;
                    this.Db.SaveChanges();
                }

                return "Instance updated successfully!";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        /// <summary>
        /// NON CRUD: Get all shop in Budapest.
        /// </summary>
        /// <returns>IEnumerabley(List).</returns>
        public IEnumerable<Shop> GetAllInBudapest()
        {
            List<Shop> result = this.Db.Shops.Where(x => x.Zipcode.StartsWith("1")).ToList();
            return result;
        }
    }
}
