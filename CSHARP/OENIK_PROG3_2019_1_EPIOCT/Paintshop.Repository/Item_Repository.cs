﻿// <copyright file="Item_Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Paintshop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Paintshop.Data;

    /// <summary>
    /// Repository to manage item table.
    /// </summary>
    public class Item_Repository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Item_Repository"/> class.
        /// </summary>
        public Item_Repository()
        {
            this.Db = new Paintshop_dbEntities1();
        }

        /// <summary>
        /// Gets or sets database instance.
        /// </summary>
        private Paintshop_dbEntities1 Db { get; set; }

        /// <summary>
        /// Delete item.
        /// </summary>
        /// <param name="id">ID to instance.</param>
        /// <returns>Returne message.</returns>
        public string Delete(int id)
        {
            // Should check if 'id' is a valid id to a instance.
            using (Paintshop_dbEntities1 db = new Paintshop_dbEntities1())
            {
                var delete = db.Items.Find(id);
                try
                {
                    db.Items.Remove(delete);
                    db.SaveChanges();
                    return "Instance removed successfully!";
                }
                catch (Exception e)
                {
                    return "Error: " + e.Message;
                }
            }
        }

        /// <summary>
        /// Querry all instance.
        /// </summary>
        /// <returns>IEnumerabley(List).</returns>
        public IEnumerable<Item> GetAll()
        {
            List<Item> result = this.Db.Items.ToList();
            return result;
        }

        /// <summary>
        /// Insert item.
        /// </summary>
        /// <param name="parameters">Given parameters.</param>
        /// <returns>Return message.</returns>
        public string Insert(List<string> parameters)
        {
            Item newItem = new Item
            {
                Id = int.Parse(parameters.ElementAt(0)),
                Name = parameters.ElementAt(1),
                Price = parameters.ElementAt(2),
                Perishable = int.Parse(parameters.ElementAt(3)),
                Perishable_Date = parameters.ElementAt(4),
            };
            try
            {
                this.Db.Items.Add(newItem);
                this.Db.SaveChanges();
                return "Instance addedd successfully!";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        /// <summary>
        /// Update an instance.
        /// </summary>
        /// <param name="parameters">Given parameters.</param>
        /// <returns>Return message.</returns>
        public string Update(List<string> parameters)
        {
            Item newItem = new Item
            {
                Id = int.Parse(parameters.ElementAt(0)),
                Name = parameters.ElementAt(1),
                Price = parameters.ElementAt(2),
                Perishable = int.Parse(parameters.ElementAt(3)),
                Perishable_Date = parameters.ElementAt(4),
            };
            try
            {
                var update = this.Db.Items.SingleOrDefault(x => x.Id == newItem.Id);
                if (update != null)
                {
                    update.Name = newItem.Name;
                    update.Price = newItem.Price;
                    update.Perishable = newItem.Perishable;
                    update.Perishable_Date = newItem.Perishable_Date;
                    this.Db.SaveChanges();
                }

                return "Instance updated successfully!";
            }
            catch (Exception e)
            {
                return "Error: " + e.Message;
            }
        }

        /// <summary>
        /// NON CRUD: Get all item which will be run down.
        /// </summary>
        /// <returns>IEnumerable(List).</returns>
        public IEnumerable<Item> GetAllPerishable()
        {
            List<Item> result = this.Db.Items.Where(x => x.Perishable == 1).ToList();
            return result;
        }
    }
}
