﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Paintshop.Logic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Paintshop.Data;
    using Paintshop.Repository;

    /// <summary>
    /// Logic class.
    /// </summary>
    public class Logic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            this.Customer = new Customer_Repository();
            this.Shop = new Shop_Repository();
            this.Item = new Item_Repository();
        }

        /// <summary>
        /// Gets or sets customers.
        /// </summary>
        public Customer_Repository Customer { get; set; }

        /// <summary>
        /// Gets or sets shop.
        /// </summary>
        public Shop_Repository Shop { get; set; }

        /// <summary>
        /// Gets or sets item.
        /// </summary>
        public Item_Repository Item { get; set; }

        // CRUD METHODS:
        // DELETE:

        /// <summary>
        /// Delete implementation for customers, in Logic.
        /// </summary>
        /// <param name="id">ID for instance id in DB.</param>
        /// <returns>Return message.</returns>
        public string DeleteFromCustomers(int id)
        {
            return this.Customer.Delete(id);
        }

        /// <summary>
        /// Delete implementation for shop, in Logic.
        /// </summary>
        /// <param name="id">ID for instance id in DB.</param>
        /// <returns>Return message.</returns>
        public string DeleteFromShop(int id)
        {
            return this.Shop.Delete(id);
        }

        /// <summary>
        /// Delete implementation for item, in Logic.
        /// </summary>
        /// <param name="id">ID for instance id in DB.</param>
        /// <returns>Return message.</returns>
        public string DeleteFromItem(int id)
        {
            return this.Item.Delete(id);
        }

        // READ:

        /// <summary>
        /// Get all customres.
        /// </summary>
        /// <returns>Enumerable customer (list).</returns>
        public IEnumerable<Customer> GetAllFromCustomers()
        {
            return this.Customer.GetAll();
        }

        /// <summary>
        /// Get all shops.
        /// </summary>
        /// <returns>Enumerable customer (list).</returns>
        public IEnumerable<Shop> GetAllFromShop()
        {
            return this.Shop.GetAll();
        }

        /// <summary>
        /// Get all items.
        /// </summary>
        /// <returns>Enumerable customer (list).</returns>
        public IEnumerable<Item> GetAllFromItem()
        {
            return this.Item.GetAll();
        }

        // CREATE:

        /// <summary>
        /// Insert to customers.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Returned message.</returns>
        public string CreateToCustomers(List<string> parameters)
        {
            return this.Customer.Insert(parameters);
        }

        /// <summary>
        /// Insert to shop.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Returned message.</returns>
        public string CreateToShop(List<string> parameters)
        {
            return this.Shop.Insert(parameters);
        }

        /// <summary>
        /// Insert to shop.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Returned message.</returns>
        public string CreateToItem(List<string> parameters)
        {
            return this.Item.Insert(parameters);
        }

        // UPDATE:

        /// <summary>
        /// Update impelemntation in Logic (customer).
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Return message.</returns>
        public string UpdateInCustomer(List<string> parameters)
        {
            return this.Customer.Update(parameters);
        }

        /// <summary>
        /// Update impelemntation in Logic (shop).
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Return message.</returns>
        public string UpdateInShop(List<string> parameters)
        {
            return this.Shop.Update(parameters);
        }

        /// <summary>
        /// Update impelemntation in Logic (item).
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Return message.</returns>
        public string UpdateInItem(List<string> parameters)
        {
            return this.Item.Update(parameters);
        }

        // NON CRUD:

        /// <summary>
        /// NON CRUD: Get all customers born in 'Debrecen'.
        /// </summary>
        /// <returns>Return list.</returns>
        public IEnumerable<Customer> GetAllFromDebrecen()
        {
            return this.Customer.GetAllFromDebrecen();
        }

        /// <summary>
        /// NON CRUD: Get all shops in 'Budapest'.
        /// </summary>
        /// <returns>Return list.</returns>
        public IEnumerable<Shop> GetAllInBudapest()
        {
            return this.Shop.GetAllInBudapest();
        }

        /// <summary>
        /// NON CRUD: Get all items which will be run down.
        /// </summary>
        /// <returns>Return list.</returns>
        public IEnumerable<Item> GetAllPerishable()
        {
            return this.Item.GetAllPerishable();
        }
    }
}
