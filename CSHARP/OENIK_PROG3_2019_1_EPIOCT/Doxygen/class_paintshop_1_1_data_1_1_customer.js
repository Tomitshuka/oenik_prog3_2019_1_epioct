var class_paintshop_1_1_data_1_1_customer =
[
    [ "Customer", "class_paintshop_1_1_data_1_1_customer.html#aa91c97e7337da92f2e86e737e5149d08", null ],
    [ "Date_of_birth", "class_paintshop_1_1_data_1_1_customer.html#a6e0e876bc0304db81b08194cab74bc86", null ],
    [ "Id", "class_paintshop_1_1_data_1_1_customer.html#ac787c3525cdb04ec1d62525ad09e17ac", null ],
    [ "Mothers_name", "class_paintshop_1_1_data_1_1_customer.html#a6a0a22bea4e2b0a03b738009a0ba08f0", null ],
    [ "Name", "class_paintshop_1_1_data_1_1_customer.html#aeca1be82870def75d462f23d47c1d0ed", null ],
    [ "Place_of_birth", "class_paintshop_1_1_data_1_1_customer.html#a91fb3509006fb586c609f13180637e6e", null ],
    [ "Purchases", "class_paintshop_1_1_data_1_1_customer.html#ab71eb53889645255ef219e059440f419", null ],
    [ "Regular_customer", "class_paintshop_1_1_data_1_1_customer.html#af99d899cbd60d104f0327c530435b39e", null ]
];