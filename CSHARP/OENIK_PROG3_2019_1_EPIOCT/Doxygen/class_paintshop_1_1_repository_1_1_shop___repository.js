var class_paintshop_1_1_repository_1_1_shop___repository =
[
    [ "Shop_Repository", "class_paintshop_1_1_repository_1_1_shop___repository.html#aaeb3f739bd4806c19a49bb6bbb50e8a6", null ],
    [ "Delete", "class_paintshop_1_1_repository_1_1_shop___repository.html#adb8da4068ccb7e8e1fca75c35c674b5a", null ],
    [ "GetAll", "class_paintshop_1_1_repository_1_1_shop___repository.html#a00f0943c9f9b8c44fb81dea8e57242ea", null ],
    [ "GetAllInBudapest", "class_paintshop_1_1_repository_1_1_shop___repository.html#a113345b6c30c8f9d9f87080830bf2cb2", null ],
    [ "Insert", "class_paintshop_1_1_repository_1_1_shop___repository.html#ad9d4088f1d083c463db312b44ce83577", null ],
    [ "Update", "class_paintshop_1_1_repository_1_1_shop___repository.html#aa1cea476afb4e96ef162ad37a1c9ee7f", null ],
    [ "Db", "class_paintshop_1_1_repository_1_1_shop___repository.html#aa67a1470a71416822be701d532104fa9", null ]
];