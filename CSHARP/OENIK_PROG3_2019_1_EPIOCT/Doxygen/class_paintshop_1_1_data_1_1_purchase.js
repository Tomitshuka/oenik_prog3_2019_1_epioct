var class_paintshop_1_1_data_1_1_purchase =
[
    [ "Amount", "class_paintshop_1_1_data_1_1_purchase.html#aabd884b4f2596699d08fc630bd6ee927", null ],
    [ "Customer", "class_paintshop_1_1_data_1_1_purchase.html#ae0d0ee495a5a0dd22fff6934d2adaaf1", null ],
    [ "Customer_id", "class_paintshop_1_1_data_1_1_purchase.html#a95f4bcff0dda29526072218b5060f325", null ],
    [ "Id", "class_paintshop_1_1_data_1_1_purchase.html#a4a5d24dbe0f744ba674bba178285dafa", null ],
    [ "Item", "class_paintshop_1_1_data_1_1_purchase.html#adf31f86e4e82150c034cb835452c5792", null ],
    [ "Product_id", "class_paintshop_1_1_data_1_1_purchase.html#a10e989198628daa03748d3eb9ffbb0b4", null ],
    [ "Purchase_date", "class_paintshop_1_1_data_1_1_purchase.html#a66d5baf7834851a62b5068797ac49bf7", null ],
    [ "Requested_invoice", "class_paintshop_1_1_data_1_1_purchase.html#af95269f34aea7a7a4cd5c465895cbed1", null ],
    [ "Shop", "class_paintshop_1_1_data_1_1_purchase.html#a2bc5345883196e47f558ea0207307237", null ],
    [ "Shop_id", "class_paintshop_1_1_data_1_1_purchase.html#a3b2ffa75110e389f482dc2679e06ef8b", null ]
];