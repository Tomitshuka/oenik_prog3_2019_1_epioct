var hierarchy =
[
    [ "Paintshop.Data.Class1", "class_paintshop_1_1_data_1_1_class1.html", null ],
    [ "Paintshop.Data.Customer", "class_paintshop_1_1_data_1_1_customer.html", null ],
    [ "DbContext", null, [
      [ "Paintshop.Data.Paintshop_dbEntities1", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html", null ]
    ] ],
    [ "Paintshop.Repository.IRepository< T >", "interface_paintshop_1_1_repository_1_1_i_repository.html", null ],
    [ "Paintshop.Repository.IRepository< Customer >", "interface_paintshop_1_1_repository_1_1_i_repository.html", [
      [ "Paintshop.Repository.Customer_Repository", "class_paintshop_1_1_repository_1_1_customer___repository.html", null ]
    ] ],
    [ "Paintshop.Data.Item", "class_paintshop_1_1_data_1_1_item.html", null ],
    [ "Paintshop.Repository.Item_Repository", "class_paintshop_1_1_repository_1_1_item___repository.html", null ],
    [ "Paintshop.Logic.Logic", "class_paintshop_1_1_logic_1_1_logic.html", null ],
    [ "Paintshop.Logic.Tests.LogicTest", "class_paintshop_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "OENIK_PROG3_2019_1_EPIOCT.Menu", "class_o_e_n_i_k___p_r_o_g3__2019__1___e_p_i_o_c_t_1_1_menu.html", null ],
    [ "Paintshop.Data.Purchase", "class_paintshop_1_1_data_1_1_purchase.html", null ],
    [ "Paintshop.Data.Shop", "class_paintshop_1_1_data_1_1_shop.html", null ],
    [ "Paintshop.Repository.Shop_Repository", "class_paintshop_1_1_repository_1_1_shop___repository.html", null ]
];