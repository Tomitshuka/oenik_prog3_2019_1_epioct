var class_paintshop_1_1_logic_1_1_logic =
[
    [ "Logic", "class_paintshop_1_1_logic_1_1_logic.html#add5033d15e0d063bd468efcc1b04fc3c", null ],
    [ "CreateToCustomers", "class_paintshop_1_1_logic_1_1_logic.html#a4731603532165ea2e94a56d32ffc04d8", null ],
    [ "CreateToItem", "class_paintshop_1_1_logic_1_1_logic.html#a18b41722b6ad9ea53695056397c0644b", null ],
    [ "CreateToShop", "class_paintshop_1_1_logic_1_1_logic.html#a09bab3b9c1ef4caf74b56af3417eeca9", null ],
    [ "DeleteFromCustomers", "class_paintshop_1_1_logic_1_1_logic.html#a4d3f36a9c2466622472dc3b23c3115e8", null ],
    [ "DeleteFromItem", "class_paintshop_1_1_logic_1_1_logic.html#a9e186664cf603ecd33d33f45f7124468", null ],
    [ "DeleteFromShop", "class_paintshop_1_1_logic_1_1_logic.html#ad38d6658b3cde4dd093a68bf02e98186", null ],
    [ "GetAllFromCustomers", "class_paintshop_1_1_logic_1_1_logic.html#a342b885f4883ed589ae081aac8a3c12f", null ],
    [ "GetAllFromDebrecen", "class_paintshop_1_1_logic_1_1_logic.html#a9d96b3567960b202f520af73baf83de5", null ],
    [ "GetAllFromItem", "class_paintshop_1_1_logic_1_1_logic.html#a4cd6d1ace094d6d36b4b86927ad24255", null ],
    [ "GetAllFromShop", "class_paintshop_1_1_logic_1_1_logic.html#aad62a8d96e556a64e0935224011b5d29", null ],
    [ "GetAllInBudapest", "class_paintshop_1_1_logic_1_1_logic.html#ad04c4c2aebd86cb249f28fb5437a6fb0", null ],
    [ "GetAllPerishable", "class_paintshop_1_1_logic_1_1_logic.html#a9e173d97b3f9f997ef1ed1f2a85a6c51", null ],
    [ "UpdateInCustomer", "class_paintshop_1_1_logic_1_1_logic.html#aaea77dd8e1a0be70162495fd56bfeebe", null ],
    [ "UpdateInItem", "class_paintshop_1_1_logic_1_1_logic.html#a7d507de4476c78e71c126bc4f8c49d37", null ],
    [ "UpdateInShop", "class_paintshop_1_1_logic_1_1_logic.html#a669ab656b40f2a4c00ee1592507ca766", null ],
    [ "Customer", "class_paintshop_1_1_logic_1_1_logic.html#a1428d7b7a44cedcc76fd7e82e7c2f822", null ],
    [ "Item", "class_paintshop_1_1_logic_1_1_logic.html#ad6157efe15759133a8e585e710b8cf3d", null ],
    [ "Shop", "class_paintshop_1_1_logic_1_1_logic.html#aa36719cef291cc8815a07ad3bd0034fe", null ]
];