var searchData=
[
  ['db',['Db',['../class_paintshop_1_1_repository_1_1_customer___repository.html#a38569931399d781bf604ce3d0e5d8055',1,'Paintshop::Repository::Customer_Repository']]],
  ['delete',['Delete',['../class_paintshop_1_1_repository_1_1_customer___repository.html#a65e83802103bbd8e4689809252594cc5',1,'Paintshop.Repository.Customer_Repository.Delete()'],['../interface_paintshop_1_1_repository_1_1_i_repository.html#a71d402aa9ce629c6027f5e486f2fe480',1,'Paintshop.Repository.IRepository.Delete()'],['../class_paintshop_1_1_repository_1_1_item___repository.html#ae1741b7772d0e4a6fbc2359ee021d455',1,'Paintshop.Repository.Item_Repository.Delete()'],['../class_paintshop_1_1_repository_1_1_shop___repository.html#adb8da4068ccb7e8e1fca75c35c674b5a',1,'Paintshop.Repository.Shop_Repository.Delete()']]],
  ['deletefromcustomers',['DeleteFromCustomers',['../class_paintshop_1_1_logic_1_1_logic.html#a4d3f36a9c2466622472dc3b23c3115e8',1,'Paintshop::Logic::Logic']]],
  ['deletefromitem',['DeleteFromItem',['../class_paintshop_1_1_logic_1_1_logic.html#a9e186664cf603ecd33d33f45f7124468',1,'Paintshop::Logic::Logic']]],
  ['deletefromshop',['DeleteFromShop',['../class_paintshop_1_1_logic_1_1_logic.html#ad38d6658b3cde4dd093a68bf02e98186',1,'Paintshop::Logic::Logic']]]
];
