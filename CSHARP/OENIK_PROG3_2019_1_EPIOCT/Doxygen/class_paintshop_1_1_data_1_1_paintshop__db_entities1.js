var class_paintshop_1_1_data_1_1_paintshop__db_entities1 =
[
    [ "Paintshop_dbEntities1", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html#aca2c704e8923af34c8580a507308f6bd", null ],
    [ "OnModelCreating", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html#ab776313a525146abb3844ac0f4a2951b", null ],
    [ "Customers", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html#abb4ad39c0f4d35fff934faa2ca3284f1", null ],
    [ "Items", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html#afefc90e7843594d548ec9c9a314b74e2", null ],
    [ "Purchases", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html#a958d5a97ed71915c82153cb7a12ab30b", null ],
    [ "Shops", "class_paintshop_1_1_data_1_1_paintshop__db_entities1.html#a08bbed4917006b42adffdf9eb89d9bd5", null ]
];