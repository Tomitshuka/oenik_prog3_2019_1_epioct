var namespace_paintshop_1_1_repository =
[
    [ "Customer_Repository", "class_paintshop_1_1_repository_1_1_customer___repository.html", "class_paintshop_1_1_repository_1_1_customer___repository" ],
    [ "IRepository", "interface_paintshop_1_1_repository_1_1_i_repository.html", "interface_paintshop_1_1_repository_1_1_i_repository" ],
    [ "Item_Repository", "class_paintshop_1_1_repository_1_1_item___repository.html", "class_paintshop_1_1_repository_1_1_item___repository" ],
    [ "Shop_Repository", "class_paintshop_1_1_repository_1_1_shop___repository.html", "class_paintshop_1_1_repository_1_1_shop___repository" ]
];