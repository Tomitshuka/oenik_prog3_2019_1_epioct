var class_paintshop_1_1_data_1_1_shop =
[
    [ "Shop", "class_paintshop_1_1_data_1_1_shop.html#a19b1550ea160a5f2ebca9e827548825d", null ],
    [ "Address", "class_paintshop_1_1_data_1_1_shop.html#ad3529304059497f43088aefdb4a0177d", null ],
    [ "Id", "class_paintshop_1_1_data_1_1_shop.html#a52cc8594e97fa6bcc34f3bc499838def", null ],
    [ "Opening_hours", "class_paintshop_1_1_data_1_1_shop.html#a7be4436f085d5a7c015f6a885315686d", null ],
    [ "Purchases", "class_paintshop_1_1_data_1_1_shop.html#a960ee55be344632d4528b4666a99cc83", null ],
    [ "Settlement", "class_paintshop_1_1_data_1_1_shop.html#ac7f36b443ed9deb6cf56660d37e00849", null ],
    [ "Shopkeeper", "class_paintshop_1_1_data_1_1_shop.html#a5c185259013d70576f805fdaf50bb036", null ],
    [ "Zipcode", "class_paintshop_1_1_data_1_1_shop.html#a26dce7bc6cbe233691516dcd2b6a380a", null ]
];