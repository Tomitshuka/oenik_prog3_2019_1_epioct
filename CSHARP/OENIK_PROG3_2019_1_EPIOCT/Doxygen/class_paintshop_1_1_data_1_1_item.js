var class_paintshop_1_1_data_1_1_item =
[
    [ "Item", "class_paintshop_1_1_data_1_1_item.html#a7566ab2ac82f133370b3c266b05d2964", null ],
    [ "Id", "class_paintshop_1_1_data_1_1_item.html#a160f2f9000a16a5cfafc67ce6b6cea86", null ],
    [ "Name", "class_paintshop_1_1_data_1_1_item.html#a8eaac98ca803479cd6e440df52bebe4f", null ],
    [ "Perishable", "class_paintshop_1_1_data_1_1_item.html#ae09d2278a7300ecef6d1b80440f1cf06", null ],
    [ "Perishable_Date", "class_paintshop_1_1_data_1_1_item.html#a26f2d3e75b431cdcd1441771d7299480", null ],
    [ "Price", "class_paintshop_1_1_data_1_1_item.html#aeeff23ddf29ae57173a94aedcee38c4e", null ],
    [ "Purchases", "class_paintshop_1_1_data_1_1_item.html#a2324084d38f21cf4e6f763b29bae3c5d", null ]
];