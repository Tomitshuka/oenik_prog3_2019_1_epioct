package controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import model.CarBrand;
import model.CarModel;
import model.QueriedBrands;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CarService implements Serializable{

}
